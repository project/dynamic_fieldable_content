<?php

/**
 * @file
 * dynamic_fieldable_content module
 * 
 */

/**
 * Implementation of hook_permission()
 */
function dynamic_fieldable_content_permission() {
  return array(
    'administer dynamic content' => array(
      'title' => t('Administer dynamic content'),
      'description' => t('Add, edit and delete CTools custom stored custom content'),
    ),
    'edit own dynamic content' => array(
      'title' => t('Edit own dynamic content'),
      'description' => t('edit own CTools custom stored custom content'),
    ),
    'clone any dynamic content' => array(
      'title' => t('Clone any dynamic content'),
      'description' => t('Clone any CTools custom stored custom content'),
    ),
  );
}

function dynamic_fieldable_content_menu() {
  $items = array();
  $items['dfc/edit/%'] = array(
      'title' => 'Edit content',
      'page callback' => 'dfc_simple_edit_menu_callback',
      'page arguments' => array(2),
      'access callback' => 'dfc_simple_edit_menu_access_callback',
      'access arguments' => array(2),
  );
  return $items;
}

/**
 * Implements hook_admin_paths().
 */
function dynamic_fieldable_content_admin_paths() {
  $paths = array(
      'dfc/edit/*' => TRUE,
  );
  return $paths;
}

function dfc_simple_edit_menu_access_callback($pane_id) {
  global $user;
  module_load_include('inc', 'ctools', 'includes/export');
  $pane_content = ctools_export_crud_load('dynamic_fieldable_content', $pane_id);
  if (user_access('administer dynamic content') || (user_access('edit own dynamic content') && ($user->uid == $pane_content->uid))) {
    return TRUE;
  }else{
    return FALSE;
  }
}

function dfc_simple_edit_menu_callback($pane_id) {
  $form = drupal_get_form('dfc_simple_edit_form', $pane_id);
  return $form;
}

function dfc_simple_edit_form($form, &$form_state, $pane_id) {
  drupal_set_title("Edit ".$pane_id);
  $form['hidden_pane_id'] = array('#type' => 'hidden', '#value' => $pane_id);
  module_load_include('inc', 'dynamic_fieldable_content', 'plugins/content_types/dynamic_content_pane');
  $settings = dynamic_fieldable_content_type_get_conf(dynamic_fieldable_content_type_content_type($pane_id), array());
  $form = dynamic_fieldable_content_settings_add_more_dynamic_fields($form, $form_state, $settings, $no_js_use = FALSE, $simple_form = TRUE);
  $form['submit_button'] = array(
    '#type' => 'submit',
    '#value' => t('Save!'),
  );
  return $form;
}
function dfc_simple_edit_form_validate($form, &$form_state) {
}
function dfc_simple_edit_form_submit($form, &$form_state) {
  //dsm($form_state);
  $content = ctools_export_crud_load('dynamic_fieldable_content', $form_state['values']['hidden_pane_id']);
  $content->settings['body'] = $form_state['values']['body']['value'];
  $content->settings['repeat_body'] = $form_state['values']['repeat_body']['value'];
  // At this point $form_state['plugin']['defaults'] is not set or empty
  _dynamic_fieldable_content_type_edit_save($content, $form_state, $simple_form = TRUE);
  /*$content->settings['num_names'] = $form_state['num_names'];
  ctools_export_crud_save('dynamic_fieldable_content', $content);*/
}

function dfc_clone($machine) {
  if (!user_access('Clone any dynamic content')) {
    module_load_include('inc', 'ctools', 'includes/export');
    $old_obj = ctools_export_crud_load('dynamic_fieldable_content', $machine);
    $import_code = ctools_export_crud_export('dynamic_fieldable_content', $old_obj);
    $new_obj = ctools_export_crud_import('dynamic_fieldable_content', $import_code);
    $new_obj->name = $old_obj->name."_clone_".time();
    global $user;
    $new_obj->uid = $user->uid;
    $new_obj->admin_title = $old_obj->admin_title."_clone";
    ctools_export_crud_save('dynamic_fieldable_content', $new_obj);
    return $new_obj->name;
 }else{
   return FALSE;
 }
}

/**
 * Implementation of hook_ctools_plugin_directory() to let the system know
 * we implement task and task_handler plugins.
 */
function dynamic_fieldable_content_ctools_plugin_directory($module, $plugin) {
  if ($module == 'ctools' && $plugin == 'export_ui') {
    return 'plugins/' . $plugin;
  }
  if ($module == 'ctools' && $plugin == 'content_types') {
    return 'plugins/content_types';
  }
}

/**
 * Create callback for creating a new Dynamic Fieldable Content type.
 *
 * This ensures we get proper defaults from the plugin for its settings.
 */
function dynamic_content_type_new($set_defaults) {
  $item = ctools_export_new_object('dynamic_fieldable_content', $set_defaults);
  ctools_include('content');
  $plugin = ctools_get_content_type('dynamic_content_pane');
  $item->settings = ctools_content_get_defaults($plugin, array());
  return $item;
}

/**
 * Implementation of hook_panels_dashboard_blocks().
 *
 * Adds page information to the Panels dashboard.
 */
function dynamic_fieldable_content_panels_dashboard_blocks(&$vars) {
  $vars['links']['dynamic_fieldable_content'] = array(
    'title' => l(t('Dynamic content'), 'admin/structure/dynamic-content/add'),
    'description' => t('Custom content panes are basic HTML you enter that can be reused in all of your panels.'),
  );

   // Load all mini panels and their displays.
  ctools_include('export');
  $items = ctools_export_crud_load_all('dynamic_fieldable_content');
  $count = 0;
  $rows = array();

  foreach ($items as $item) {
    $rows[] = array(
      check_plain($item->admin_title),
      array(
        'data' => l(t('Edit'), "admin/structure/dynamic-content/list/$item->name/edit"),
        'class' => 'links',
      ),
    );

    // Only show 10.
    if (++$count >= 10) {
      break;
    }
  }

  if ($rows) {
    $content = theme('table', array('rows' => $rows, 'attributes' => array('class' => 'panels-manage')));
  }
  else {
    $content = '<p>' . t('There are no custom content panes.') . '</p>';
  }

  $vars['blocks']['dynamic_fieldable_content'] = array(
    'title' => t('Manage dynamic content'),
    'link' => l(t('Go to list'), 'admin/structure/dynamic-content'),
    'content' => $content,
    'class' => 'dashboard-content',
    'section' => 'right',
  );
}
